import { act, render } from "@testing-library/react";

import DocViewer from "..";

test("renders component with no documents", () => {
  const comp = render(<DocViewer documents={[]} />);
  expect(comp?.getByTestId("react-doc-viewer")).toMatchSnapshot();
});

test.only("renders component with no file name", async () => {
  let comp = render(
    <DocViewer
      // documents={[{ uri: require("../_example-files_/pdf.pdf") }]}
      documents={[{ uri: "https://www.soundczech.cz/temp/lorem-ipsum.pdf" }]}
      config={{ header: { disableFileName: true } }}
    />
  );
// console.log(comp);
  act(async () => {
    await comp?.findByTestId("file-name");
  });

  // expect(comp?.getByTestId("react-doc-viewer")).toMatchSnapshot();
});

test("renders component with no header", async () => {
  let comp = render(
    <DocViewer
      documents={[{ uri: require("../_example-files_/pdf.pdf") }]}
      config={{ header: { disableHeader: true } }}
    />
  );

  act(async () => {
    await comp?.findByTestId("header-bar");
  });

  expect(comp?.getByTestId("react-doc-viewer")).toMatchSnapshot();
});
